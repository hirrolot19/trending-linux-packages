package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"os"
	"sort"
)

const (
	apiURL     = "https://pkgstats.archlinux.de/api/packages"
	batchSize  = 200
	acceptJSON = "application/json"
)

type PackagePopularity struct {
	Name           string  `json:"name"`
	Popularity     float64 `json:"popularity"`
	PopularityData []struct {
		Popularity float64 `json:"popularity"`
	} `json:"popularityData"`
}

type PackageData struct {
	Total              int                `json:"total"`
	PackagePopularities []PackagePopularity `json:"packagePopularities"`
}

type PackageWithScore struct {
	Name  string
	Score float64
}

func getPackageData(limit, offset int) (PackageData, error) {
	url := fmt.Sprintf("%s?limit=%d&offset=%d", apiURL, limit, offset)
	resp, err := http.Get(url)
	if err != nil {
		return PackageData{}, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return PackageData{}, err
	}

	var data PackageData
	err = json.Unmarshal(body, &data)
	if err != nil {
		return PackageData{}, err
	}

	return data, nil
}

func calculateTotalGain(pkg PackagePopularity) float64 {
	current := pkg.Popularity
	prev := pkg.PopularityData[len(pkg.PopularityData)-7].Popularity
	return current - prev
}

func calculateRelativeGain(pkg PackagePopularity) float64 {
	current := pkg.Popularity
	prev := pkg.PopularityData[len(pkg.PopularityData)-7].Popularity
	if prev == 0 {
		return math.Inf(1)
	}
	return (current - prev) / prev * 100
}

func calculateLogGain(pkg PackagePopularity) float64 {
	current := pkg.Popularity
	prev := pkg.PopularityData[len(pkg.PopularityData)-7].Popularity
	return math.Log(current) - math.Log(prev)
}

func calculateCombinedScore(pkg PackagePopularity) float64 {
	totalGain := calculateTotalGain(pkg)
	relativeGain := calculateRelativeGain(pkg)
	prevPopularity := pkg.PopularityData[len(pkg.PopularityData)-7].Popularity
	return (totalGain + (relativeGain * prevPopularity / 100)) / 2
}

func sortPackages(packages []PackagePopularity, sortingMethod string) []PackageWithScore {
	var packagesWithScore []PackageWithScore

	for _, pkg := range packages {
		var score float64
		switch sortingMethod {
		case "total":
			score = calculateTotalGain(pkg)
		case "relative":
			score = calculateRelativeGain(pkg)
		case "log":
			score = calculateLogGain(pkg)
		case "combined":
			score = calculateCombinedScore(pkg)
		default:
			log.Fatalf("Invalid sorting method: %s", sortingMethod)
		}
		packagesWithScore = append(packagesWithScore, PackageWithScore{pkg.Name, score})
	}

	sort.Slice(packagesWithScore, func(i, j int) bool {
		return packagesWithScore[i].Score > packagesWithScore[j].Score
	})

	return packagesWithScore
}

func sortPackagesTiered(packages []PackagePopularity) []PackageWithScore {
	var packagesWithScore []PackageWithScore

	for _, pkg := range packages {
		totalGain := calculateTotalGain(pkg)
		relativeGain := calculateRelativeGain(pkg)
		packagesWithScore = append(packagesWithScore, PackageWithScore{pkg.Name, totalGain + relativeGain/100000})
	}

	sort.Slice(packagesWithScore, func(i, j int) bool {
		if packagesWithScore[i].Score == packagesWithScore[j].Score {
			return calculateRelativeGain(packages[i]) > calculateRelativeGain(packages[j])
		}
		return packagesWithScore[i].Score > packagesWithScore[j].Score
	})

	return packagesWithScore
}

func loadPackagesFromFile(filename string) ([]PackagePopularity, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var packages []PackagePopularity
	if err := json.NewDecoder(file).Decode(&packages); err != nil {
		return nil, err
	}

	return packages, nil
}

func savePackagesToFile(filename string, packages []PackagePopularity) error {
	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	encoder := json.NewEncoder(file)
	encoder.SetIndent("", "  ")
	return encoder.Encode(packages)
}

func main() {
	sortingMethod := flag.String("sort", "total", "Sorting method (total, relative, log, combined, tiered)")
	useCache := flag.Bool("cache", false, "Use cached package popularities from a file")
	cacheFile := flag.String("cache-file", "package-popularities.json", "File to cache/load package popularities")
	flag.Parse()

	var packages []PackagePopularity
	var err error

	if *useCache {
		packages, err = loadPackagesFromFile(*cacheFile)
		if err != nil {
			log.Printf("Failed to load package popularities from %s: %v", *cacheFile, err)
		}
	}

	if len(packages) == 0 {
		offset := 0
		for {
			data, err := getPackageData(batchSize, offset)
			if err != nil {
				log.Fatalf("Failed to fetch package data: %v", err)
			}

			packages = append(packages, data.PackagePopularities...)
			offset += batchSize

			if offset >= data.Total {
				break
			}
		}

		if *useCache {
			if err := savePackagesToFile(*cacheFile, packages); err != nil {
				log.Printf("Failed to save package popularities to %s: %v", *cacheFile, err)
			}
		}
	}

	var sortedPackages []PackageWithScore
	switch *sortingMethod {
	case "total", "relative", "log", "combined":
		sortedPackages = sortPackages(packages, *sortingMethod)
	case "tiered":
		sortedPackages = sortPackagesTiered(packages)
	default:
		log.Fatalf("Invalid sorting method: %s", *sortingMethod)
	}

	for _, pkg := range sortedPackages {
		fmt.Println(pkg.Name)
	}
}
